//
// LLCS Runtime, version $Version$.
//
// Copyright (c) 2021, The Aspie Project <http://aspie.ru/>
// All rights reserved.
//

namespace System
{
	public interface IEquatable<T>
	{
		public bool Equals(T? other);
	}
}
