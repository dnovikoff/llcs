//
// LLCS Runtime, version $Version$.
//
// Copyright (c) 2021, The Aspie Project <http://aspie.ru/>
// All rights reserved.
//

namespace System
{
	public interface IComparable
	{
		public int CompareTo(object? other);
	}

	public interface IComparable<in T>
	{
		public int CompareTo(T? other);
	}
}
