/**
 * LLCS, version $Version$.
 *
 * Copyright (c) 2021, The Aspie Project <http://aspie.ru/>
 * All rights reserved.
 */
#ifndef __LLCS_NAME_H__
#define __LLCS_NAME_H__

#ifndef __LLCS_NAME_T__
#define __LLCS_NAME_T__
typedef struct name_st name_t;
#endif /* !__LLCS_NAME_T__ */

struct name_st
{
	int          n_id;
	const char * n_name;
};

#endif /* !__LLCS_NAME_H__ */
